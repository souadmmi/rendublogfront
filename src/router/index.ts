import { createRouter, createWebHistory } from 'vue-router'
import Accueil from '../views/Accueil.vue'
import OneArticle from '@/views/OneArticle.vue'
import Categorie from '@/views/Categorie.vue'
import AddArticle from '@/views/AddArticle.vue'
import Login from '@/views/Login.vue'
import UserPage from '@/views/UserPage.vue'
import Register from '@/views/Register.vue'
import AllUsers from '@/views/AllUsers.vue'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'accueil',
            component: Accueil
        },
        {
            path: '/article/:id',
            name: 'oneArticle',
            component: OneArticle
        },
        {
            path: '/categorie/:id',
            name: 'Categorie',
            component: Categorie
        },
        {
            path: '/add-article',
            name: 'AddArticle',
            component: AddArticle
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/pageUser',
            name :'pageUser',
            component: UserPage
        },
        {
            path:'/register',
            name:'register',
            component: Register
        },
        {
            path:'/allUsers',
            name:'allUsers',
            component: AllUsers
        }
    ]


})

export default router